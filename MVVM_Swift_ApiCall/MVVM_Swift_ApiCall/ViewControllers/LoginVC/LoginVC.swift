

import UIKit

class LoginVC: UIViewController,LoginViewModelDelegate  {
    
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    var loginVM = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginVM.delegate = self
        activityLoader.isHidden = true
    }
    
    //MARK: Login Button Action
    @IBAction func LoginButtonAction(_ sender: Any) {
        let message = self.validation()
        if message != "" {
            self.showAlertView(msg: message)
        } else {
            showHideActivityLoder(isHide: false)
            activityLoader.startAnimating()
            loginVM.callApi()
        }
    }
    
    //MARK: Login Response Delegate method
    func loginApiResponse(isSuccess: Bool) {
        showHideActivityLoder(isHide: true)
        if isSuccess {
            DispatchQueue.main.async {
                let second = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SecondVC") as! SecondVC
                self.navigationController?.pushViewController(second, animated: true)
            }
        } else {
            self.showAlertView(msg: "Login Failed !")
        }
    }
}

extension LoginVC {
    
    //MARK: Login TextFields Validation Method
    func validation() -> String {
        if self.userNameTextField.text == StringContant.emptyString {
            return StringContant.email
        }
        if self.passwordTextField.text == StringContant.emptyString {
            return StringContant.password
        }
        return StringContant.emptyString
    }
    
    //MARK: Hide/Show Loader
    func showHideActivityLoder(isHide: Bool) {
        DispatchQueue.main.async {
            self.activityLoader.isHidden = isHide
        }
    }
    
    //MARK: Alertview
    func showAlertView(msg: String) {
        let refreshAlert = UIAlertController(title: msg, message: "", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    
    
}
