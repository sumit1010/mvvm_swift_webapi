 

import Foundation

// MARK: - TopLevel
struct LoginModel: Codable {
   let success: Bool
   let message: String
   let employee: Employee
}

// MARK: - Employee
struct Employee: Codable {
   let empID: Int
   let name: String
   let salary: Int
   let married: Bool

   enum CodingKeys: String, CodingKey {
       case empID = "emp_id"
       case name, salary, married
   }
}
