
import UIKit

@objc protocol LoginViewModelDelegate {
    @objc optional func loginApiResponse(isSuccess: Bool)
}

class LoginViewModel: LoginViewModelDelegate {
    var delegate: LoginViewModelDelegate?
    
    init() {
        print("Login view model is initialized...")
    }
    
    func callApi() {
        //        loaderStatus(loading: true)
        ApiHandler.getApiCall(serverUrl: StringContant.loginApiUrl, for: LoginModel.self) { [self] result in
            
            switch result {
            case .success(let response):
                if response.success {
                    self.delegate?.loginApiResponse?(isSuccess: true)
                } else {
                    self.delegate?.loginApiResponse?(isSuccess: false)
                }
            case .failure(let error):
                print("Login api response is: \(error.localizedDescription)")
                self.delegate?.loginApiResponse?(isSuccess: false)
            }
        }
    }
     
}
