 
import Foundation
struct StringContant {
    
    //MARK: Login constants
    public static var emptyString = ""
    public static var email = "Enter email"
    public static var validEmail = "Enter valid email"
    public static var password = "Enter Password"
    public static var passwordTooShort = "Password is too short"
    public static var loginButtonTitle = "Login"
    public static var loginFailed = "Sorry, login failed"
    
    //MARK: Dashboard constants
    public static var dashBoardWelcomeMessage = "Welcome To Dashboard"
    public static var dashBoardNavigationTitle = "Dashboard"
    
    //MARK: AlertView Constants
    public static var okTitle = "Ok"
    public static var activityLoaderText = "Please wait..."
    
    //MARK: Email Regex constants
    public static var emailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    
    //MARK: API URL's
    public static var loginApiUrl = "https://mocki.io/v1/8ff71f57-9ee5-40d0-b2b7-6d8b7d2530ea"
    
}
